<?php
define("WEBDIR",get_template_directory());
define("WEBURL",get_template_directory_uri());

function futureEd_initConfig(){
    add_theme_support( 'menus' );
    /*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 825, 510, true );
    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu',      'twentyfifteen' ),
        'social'  => __( 'Social Links Menu', 'twentyfifteen' ),
    ) );
    /*
	 * Enable support for custom logo.
	 *
	 * @since Twenty Fifteen 1.5
	 */
    add_theme_support( 'custom-logo', array(
        'height'      => 248,
        'width'       => 248,
        'flex-height' => true,
    ) );

    add_theme_support( 'customize-selective-refresh-widgets' );
}

add_action("init",'futureEd_initConfig');

/**
 * Register widget area.
 *
 * @since FutureEd 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function futureEd_sidebar(){
    register_sidebar( array(
        'name'          => __( 'Register Sidebar', 'twentyfifteen' ),
        'id'            => 'register-sidebar',
        'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
        'before_widget' => '<aside id="%1$s" class="widget register_sidebar col-md-4 %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h5 class="widget-title">',
        'after_title'   => '</h5>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Register content', 'twentyfifteen' ),
        'id'            => 'register-bottom-content',
        'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
        'before_widget' => '<div class="col-lg-6 register_custom_text">
                                <div class="row">
                                <span class="fa-3x register-custom-icon col-3">
                                    <span class="fa-layers fa-fw">
                                    <i class="fas fa-circle"></i>
                                    <i class="fa-inverse fas fa-arrow-right" data-fa-transform="shrink-6"></i>
                                    </span>
                                </span>
                                <div id="%1$s" class="widget col-9 %2$s">',
        'after_widget'  => '</div></div></div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );
}

add_action("widgets_init","futureEd_sidebar");

function sidebar_shortcode( $atts ) {
    $defaults = shortcode_atts( array(
        'name' => 'something',
    ), $atts );
    if(is_active_sidebar($defaults["name"])){
        return dynamic_sidebar($defaults["name"]);
    }
    else{
        return false;
    }
}
add_shortcode( 'futureEd_widget', 'sidebar_shortcode' );

function twentyfifteen_scripts() {
    // Add custom fonts, used in the main stylesheet.
    wp_enqueue_style( 'custom-theme', WEBURL."/css/custom_theme.css", array(), null );
    wp_enqueue_style( 'fontawesome_custom', "https://use.fontawesome.com/releases/v5.0.11/css/all.css", array(), '5.0.11' );
    wp_enqueue_style( 'stylesheet_custom', WEBURL."/style.css", array(), null );
    wp_enqueue_script("fontawesome-script","https://use.fontawesome.com/releases/v5.0.11/js/all.js");

}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_scripts' );

function admin_fonts(){
    wp_enqueue_script("admin-google-font","https://fonts.googleapis.com/css?family=Open+Sans:400,800");
}

add_action('admin_init','admin_fonts');

add_action( 'wp_footer', 'redirect_cf7' );

function redirect_cf7() {
    ?>
    <script type="text/javascript">
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            location = '<?php echo get_site_url(); ?>/thank-you';
        }, false );
    </script>
    <?php
}


