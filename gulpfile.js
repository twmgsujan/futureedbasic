let gulp = require('gulp');
let sass = require('gulp-sass');
// gulp.task('styles', function() {
//     gulp.src('sass/**/*.scss')
//         .pipe(sass().on('error', sass.logError))
//         .pipe(gulp.dest('./css/'))
// });
gulp.task('styles', function() {
    gulp.src('sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css/'))
});

gulp.task('watch', function() {
    // gulp.watch('js/**/*.js', ['scripts']);
    gulp.watch('sass/*.scss', ['styles']);
});
//Watch task
gulp.task('default',['watch']);