<?php
/**
 * Template Name: Thank You Page
 *
 * @package FutureEd
 * @since FutureEd 1.0
 */
?>
<?php get_header(); ?>
    <section class="container">
        <div class="row register_page">
            <article class="col-12">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <!-- Test if the current post is in category 3. -->
                    <!-- If it is, the div box is given the CSS class "post-cat-three". -->
                    <!-- Otherwise, the div box is given the CSS class "post". -->
                    <div class="thank_page">
                        <div class="img-container">
                            <?php the_post_thumbnail('large',array('class'=>'img img-responsive')) ?>
                            <?php the_title('<h1 class="page_title">',"</h1>") ?>
                        </div>
                        <div class="content">
                            <?php the_content(); ?>
                        </div>

                    </div>
                <?php endwhile; ?>
                <?php else : ?>
                    <h3><?php _e('404 Error&#58; Not Found'); ?></h3>
                <?php endif; ?>
            </article>
        </div>
    </section>
<?php get_footer(); ?>