<?php
/**
 * Template Name: Register Page
 *
 * @package FutureEd
 * @since FutureEd 1.0
 */
?>
<?php get_header(); ?>
<section class="container">
    <div class="row register_page">
        <article class="col-md-8 col-12">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <!-- Test if the current post is in category 3. -->
                <!-- If it is, the div box is given the CSS class "post-cat-three". -->
                <!-- Otherwise, the div box is given the CSS class "post". -->
                <div class="register-page">
                    <div class="logo">
                        <?php the_custom_logo() ?>
                    </div>
                    <div class="post">
                        <div class="entry">
                            <?php the_content(); ?>
                            <div class="d-block d-sm-none" style="margin:30px -30px;">
                                <?php dynamic_sidebar("register-sidebar") ?>
                            </div>
                        </div>
                    </div>
                    <div class="post_extra_content">
                        <div class="row">
                            <?php dynamic_sidebar("register-bottom-content") ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            <?php else : ?>
                <h3><?php _e('404 Error&#58; Not Found'); ?></h3>
            <?php endif; ?>
        </article>
        <?php dynamic_sidebar("register-sidebar") ?>
    </div>
</section>
<?php get_footer(); ?>